package com.example.retrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = findViewById(R.id.textview);

        ApiService.getData("03.06.2019")
                .enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                        if (response.isSuccessful() && response.body() != null){
                            ResponseModel.ExchangeRate rate = response.body().getExchangeRate().get(1);
                            String formattedRate = String.format(Locale.US, "Rate of %s is %.2f", rate.getCurrency(), rate.getSaleRateNB());
                            text.setText(formattedRate);
                        } else{
                            Toast.makeText(MainActivity.this, getString(R.string.SmthWentWrong), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                        Toast.makeText(MainActivity.this, getString(R.string.SmthWentWrong), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
