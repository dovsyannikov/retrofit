package com.example.retrofit;

import java.util.List;

public class ResponseModel {
    private String date;
    private String bank;
    private Integer baseCurrency;
    private String baseCurrencyLit;

    private List<ExchangeRate> exchangeRate = null;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Integer getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(Integer baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public String getBaseCurrencyLit() {
        return baseCurrencyLit;
    }

    public void setBaseCurrencyLit(String baseCurrencyLit) {
        this.baseCurrencyLit = baseCurrencyLit;
    }

    public List<ExchangeRate> getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(List<ExchangeRate> exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public class ExchangeRate {

        private String baseCurrency;

        private String currency;

        private Double saleRateNB;

        private Double purchaseRateNB;

        private Double saleRate;

        private Double purchaseRate;

        public String getBaseCurrency() {
            return baseCurrency;
        }

        public void setBaseCurrency(String baseCurrency) {
            this.baseCurrency = baseCurrency;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public Double getSaleRateNB() {
            return saleRateNB;
        }

        public void setSaleRateNB(Double saleRateNB) {
            this.saleRateNB = saleRateNB;
        }

        public Double getPurchaseRateNB() {
            return purchaseRateNB;
        }

        public void setPurchaseRateNB(Double purchaseRateNB) {
            this.purchaseRateNB = purchaseRateNB;
        }

        public Double getSaleRate() {
            return saleRate;
        }

        public void setSaleRate(Double saleRate) {
            this.saleRate = saleRate;
        }

        public Double getPurchaseRate() {
            return purchaseRate;
        }

        public void setPurchaseRate(Double purchaseRate) {
            this.purchaseRate = purchaseRate;
        }
    }
}
